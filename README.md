## Clustering Deviance Index (CDI)

This repository is for the R package of Clustering Deviance Index (CDI). 

### Installation guidance

In R, run the following block of codes:

```
remotes::install_gitlab(
  repo = "jichunxie/xie-lab-software_cdi",
  subdir = "CDI",
  host = "gitlab.oit.duke.edu",
  build_vignettes = TRUE,
  build_manual = TRUE)

```

A tutorial of this package can be found in this repository under CDI/vignettes/CDI.html or by running the following block of codes in R:

```
library(CDI)
browseVignettes("CDI")
```
### Related paper


